// Gumby is ready to go
Gumby.ready(function() {
	Gumby.log('Gumby is ready to go...', Gumby.dump());

    //swiper gallery initialisation
    var Swiper_main = new swiper('.device-main',{calculateHeight:true, autoResize:true});

    //speaker gallery initialisation
    var Swiper_speakers = new swiper('.speakers-block',{pagination: '.pagination', paginationClickable:true, calculateHeight:true, autoResize:true, resizeReInit:true});

	// placeholder polyfil
	if(Gumby.isOldie || Gumby.$dom.find('html').hasClass('ie9')) {
		$('input, textarea').placeholder();
	}

	// skip link and toggle on one element
	// when the skip link completes, trigger the switch
	$('#skip-switch').on('gumby.onComplete', function() {
		$(this).trigger('gumby.trigger');
	});

// Oldie document loaded
}).oldie(function() {
	Gumby.warn("This is an oldie browser...");

// Touch devices loaded
}).touch(function() {
	Gumby.log("This is a touch enabled device...");
});

//object for swiper
function swiper(selector, params) {
//selector - parent selector of swiper-container
//params obj with params {param:value,...} function() {
    this.arrowsShow = function () {
        var selector=this.selector;
        if ($(selector+' .swiper-slide:first-child').hasClass('swiper-slide-visible'))
            $(selector+' .arrow-left').css({'visibility':'hidden'});
        else
            $(selector+' .arrow-left').css({'visibility':'visible'});

        if ($(selector+' .swiper-slide:last-child').hasClass('swiper-slide-visible'))
            $(selector+' .arrow-right').css({'visibility':'hidden'});
        else
            $(selector+' .arrow-right').css({'visibility':'visible'});
    }
    this.swiper_resize = function () {
        if ($(window).width()<1020) {
            $(this.selector+" .arrows").addClass('narrow');
            $(this.selector+" .arrows").css('top', "");
        }
        else {
            $(this.selector+" .arrows").removeClass('narrow');
            $(this.selector+" .arrows").css('top', this.name.height/2);
        }
    }
    params['grabCursor']=true;
    this.selector = selector;
    this.name = new Swiper(this.selector+" .swiper-container", params);
    var self_= this;
    this.name.params.onTouchEnd = function() {
        self_.arrowsShow();
    };
    $(self_.selector+' .arrow-left').on('click', function(e){
        e.preventDefault();
        self_.name.swipePrev();
        self_.arrowsShow();
    });
    $(self_.selector+' .arrow-right').on('click', function(e){
        e.preventDefault();
        self_.name.swipeNext();
        self_.arrowsShow();
    });
    $(this.selector+' .pagination').on('click', function(e) {
        self_.arrowsShow();
    });
    this.arrowsShow();
    this.swiper_resize();
    $(window).on('resize',function () {
        self_.swiper_resize();
    });
}